from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Oficina, Paquete
from .forms import OficinaForm, PaqueteForm
from django.contrib import messages


def home(request):
    return render(request, 'paquetesApp/home.html', {})

def oficinas(request):
    oficinas= Oficina.objects.all();
    form = OficinaForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            oficina = form.save(commit=False)
            oficina.save()
            messages.success(request, ('Oficina creada correctamente!'))
            return redirect("paquetesApp:oficinas")
    else:
        return render(request,  'paquetesApp/oficinas.html',{"form":form, "oficinas":oficinas})

def paquetes(request):
    paquetes = Paquete.objects.all().order_by("-created_at")
    form = PaqueteForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            paquete = form.save(commit=False)
            paquete.save()
            return redirect("paquetesApp:paquetes") 
    return render(request, 'paquetesApp/paquetes.html',{"form": form, "paquetes": paquetes})

def liquidar(request, paquete_id):
    paquete = Paquete.objects.get(id=paquete_id)
    paquete.estado= "L"
    paquete.save()
    messages.success(request, (f'{paquete.codigo}'+ " liquidado correctamente"))
    return redirect("paquetesApp:paquetes")
   
# dwitter/forms.py

from django import forms
from .models import Oficina,Paquete

class OficinaForm(forms.ModelForm):
    ciudad = forms.CharField(
        required=True,
        widget=forms.widgets.TextInput(
            attrs={
                "placeholder": "Ciudad",
                "class": "input is-success is-medium is-round mr-6",
                "value": "San Francisco"
            }
        ),
        label="",
    )        
    estado = forms.CharField(
        required=True,
        widget=forms.widgets.TextInput(
            attrs={
                "placeholder": "Estado",
                "class": "input is-success is-medium is-round mr-6",
            }
        ),
        label="",
    )

    nombre = forms.CharField(
        required=True,
        widget=forms.widgets.TextInput(
            attrs={
                "placeholder": "Nombre",
                "class": "input is-success is-medium is-round mr-6",
            }
        ),
        label="",
    )
    class Meta:
        model= Oficina
        fields = '__all__' 

class PaqueteForm(forms.ModelForm):
    TIPOS= (
        ('C','Alimentos'),
        ('R','Textiles/Prendas'),
        ('M','Medicamentos'),
        ('O','Otros')   
    )
    ESTADOS =(
        ('O', 'En oficina'),
        ('L', 'Liquidado'),
        ('D', 'Devolucion')
    )    
    codigo = forms.CharField(
        required=True,
        widget= forms.widgets.NumberInput(
            attrs={
                "placeholder": "Codigo Paquete",
                "class": "input is-success is-medium is-round mr-6"
            }
        ),
        label=""

    )
    office = forms.ModelChoiceField(
        required= True,
        to_field_name= 'id',
        queryset= Oficina.objects.all(),
        label=""
    )
    tipo = forms.ChoiceField(
        required= True, 
        choices= TIPOS, 
        label=""
    )
    nombre_destinatario= forms.CharField(
        required=False,
        widget=forms.widgets.TextInput(
            attrs={
                "placeholder": "Destinatario",
                "class": "input is-success is-medium is-round mr-6"
            }
        ),
        label="",
    )   
    telefono_destinatario= forms.CharField(
        required=False,
        widget=forms.widgets.TextInput(
            attrs={
                "placeholder": "Telefono",
                "class": "input is-success is-medium is-round mr-6"
            }
        ),
        label="",
    ) 
    peso = forms.DecimalField(
        max_value= 10000,
        max_digits= 4,
        required=False, 
        widget= forms.widgets.NumberInput(
            attrs={
                "placeholder": "Peso (lb)",
                "class": "input is-success is-medium is-round mr-6"
            }
            ),
            label=""


    )
    estado = forms.ChoiceField(
        required= True, 
        choices= ESTADOS, 
        label=""
    )    
    class Meta:
        model= Paquete
        fields = '__all__'         
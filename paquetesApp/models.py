from datetime import date
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Oficina(models.Model):
    estado = models.CharField(max_length=256)
    ciudad = models.CharField(max_length=256)
    nombre = models.CharField(max_length=256)
    # contacto= models.CharField(max_length=256)
    # telefono = models.CharField(max_length=256)

    def __str__(self):
        return self.nombre

class Paquete(models.Model):
    TIPOS= (
        ('C','Alimentos'),
        ('R','Textiles/Prendas'),
        ('M','Medicamentos'),
        ('O','Otros')   
    )
    ESTADO =(
        ('O', 'En oficina'),
        ('L', 'Liquidado'),
        ('D', 'Devolucion')
    )
    codigo= models.CharField(max_length=10,unique=True)
    office = models.ForeignKey( Oficina, related_name="paquetes", on_delete=models.DO_NOTHING)
    tipo = models.CharField(max_length=1, choices=TIPOS)
    nombre_destinatario = models.CharField(max_length=256,null=True)
    telefono_destinatario=models.CharField(max_length=20, null=True)
    peso= models.IntegerField(null=True)
    estado= models.CharField(choices= ESTADO, max_length=1,default='O')
    created_at= models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.codigo   







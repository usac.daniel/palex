from django.urls import path
from . import views

app_name = "paquetesApp"
urlpatterns = [
    path('',views.home, name='home'),
    path('oficinas/', views.oficinas,name='oficinas'),
    path('paquetes/', views.paquetes, name='paquetes'),
    path('liquidar/<int:paquete_id>', views.liquidar, name='liquidar')
]
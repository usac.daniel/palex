from django.apps import AppConfig


class PaquetesappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'paquetesApp'
